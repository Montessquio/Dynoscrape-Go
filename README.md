# Dynoscrape-Go

A mod-log scraper custom built for a discord community.
Outputs data to JSON or CSV.

## Requirements

For compiling, the GO compiler and tools are required to be installed.

## Installation

To build the binary, simply run `go build -o ./out/dynoscrape ./src/...` from within the repository.

Windows hosts may use the slightly modified version: `go build -o ./out/dynoscrape.exe ./src/...`.

## Usage

Fill out the configuration TOML file. A sample one can be printed on STDOUT using the `-e` flag.

Then, run the program with the `-c <PATH/TO/YOUR/CONFIG/TOML/HERE>` flag.