.PHONY: all deps win
all: deps
	go build -o ./out/dynoscrape ./src/...

win32: deps
	GOOS=windows GOARCH=386 go build -o ./out/dynoscrape.exe ./src/...

win64: deps
	GOOS=windows GOARCH=amd64 go build -o ./out/dynoscrape.exe ./src/...

deps:
	go get github.com/BurntSushi/toml
	go get github.com/bwmarrin/discordgo
	go get github.com/gocarina/gocsv