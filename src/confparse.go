package main 

import (
	// External Imports
	"github.com/BurntSushi/toml"

	// stdlib imports
	"io/ioutil"
	"errors"
)

// Config struct matches to TOML Config file.
type config struct {
	Token string `toml:"token"`
	Channel string `toml:"channel"`
	
	MaxMessages int `toml:"max_msgs"`

	Path string `toml:"out_path"`
	Format string `toml:"out_format"`
}

func loadConfig(path string) (*config, error) {
	// Read config file
	dat, err := ioutil.ReadFile(path)
    if err != nil { return nil, err }

	var conf config
	_, err = toml.Decode(string(dat), &conf)
	if err != nil { return nil, err }

	if (conf.Format != "csv") && (conf.Format != "json") {
		return nil, errors.New(`out_format must be either "csv" or "json"`)
	}

	if conf.MaxMessages < 0 { 
		return nil, errors.New(`max_msgs must not be negative`)
	}

	return &conf, nil
}

const exampleToml string = `
# This is an example TOML file for the bot config.
# Please read all fields carefully as they set how the bot behaves.

# Bot auth token.
token = "TOKEN_HERE"

# This is the channel within the server to scrape from. This is NOT the channel's name.
channel = "YOUR_LOG_CHANNEL_ID_HERE"

# How many messages to scrape. Set to 0 for all of them.
max_msgs = 200

# This is a path (may be full or relative) to the output file.
out_path = "/path/to/your/output.csv"

# Set the output format. May be "csv" or "json"
out_format = "csv"
`