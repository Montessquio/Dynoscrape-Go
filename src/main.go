package main

import (
	// stdlib imports
	"os"
	"fmt"
)

func main() {

	args := os.Args[1:]

	// Check to see if there are no arguments or if the argument is -h
	if (len(args) == 0) || args[0] == "-h" || args[0] == "--help" {
		printHelp()
		os.Exit(0)
	}

	if args[0] == "-e" || args[0] == "--example" {
		fmt.Print(exampleToml)
		os.Exit(0)
	}

	if args[0] == "-c" || args[0] == "--config" {
		if len(args) == 1 || args[1] == "" { 
			printHelp()
			os.Exit(0)
		}

		// At this point we must have at least something valid.
		conf, err := loadConfig(args[1]) 
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		// Botmain is treated as essentially a different program. All the stuff before is loader code.
		// Botmain will exit, panic, and handle errors all on it's own. It should have no interaction
		// with main() code except to receive the config struct.
		_BOTMAIN(conf)
	}

	printHelp()
	os.Exit(0)
}

func printHelp() {
	fmt.Printf("Usage of %s:\n    %s <OPTIONS>\n\n", os.Args[0], os.Args[0])
	fmt.Printf("Valid Options:\n    -h    Print this help and exit.\n    -e    Write an example configuration file to stdout and exit.\n    -c <CONFIG>    Specify a config file to use and run. ")
}