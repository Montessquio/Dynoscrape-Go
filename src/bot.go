package main

import (
	"github.com/bwmarrin/discordgo"

	"fmt"
	"os"
	"encoding/json"
	"io/ioutil"
)

func _BOTMAIN(conf *config) {

	// Create bot session.
	discord, err := discordgo.New("Bot " + conf.Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		os.Exit(1)
	}

	// Open a websocket connection to Discord and begin listening.
	err = discord.Open()
	if err != nil {
		fmt.Println("error opening connection: ", err)
		os.Exit(1)
	}
	defer discord.Close()

	// Set presence.
	discord.UpdateStatus(0, "Performing duties...")

	var last string = "" // Last message in loop. Used in ""
	//messages, err := discord.ChannelMessages(conf.Channel, 20, last, "", "")
	if err != nil {
		fmt.Println("error opening connection: ", err)
		os.Exit(1)
	}


	// If we get 100 messages per run, so divide maxmsgs by 100
	var messages []*discordgo.Message

	// If the max messages is zero, set it to the maximum.
	if conf.MaxMessages == 0 {
		for i := 0; i < int(^uint(0) >> 1); i++ {
			tmp, err := discord.ChannelMessages(conf.Channel, 100, last, "", "")
			if err != nil {
				fmt.Println("error reading batch messages: ", err)
				os.Exit(1)
			}
	
			if len(tmp) == 0 {
				// If there are no more messages, just stop querying.
				break
			}
	
			messages = append(messages, tmp...)
			last = tmp[len(tmp)-1].ID
		}

		// Easy workaround to prevent some state info from being unnecessarily saved.
		goto End
	}

	for i := 0; i < int(conf.MaxMessages / 100) + 1; i++ {
		tmp, err := discord.ChannelMessages(conf.Channel, 100, last, "", "")
		if err != nil {
			fmt.Println("error reading batch messages: ", err)
			os.Exit(1)
		}

		if len(tmp) < 99 {
			// If there are less than 100 messages, just handle them in extras.
			break
		}

		messages = append(messages, tmp...)
		last = tmp[len(tmp)-1].ID
	}

	// Now grab extras.
	if (conf.MaxMessages%100) > 0 {
		tmp, err := discord.ChannelMessages(conf.Channel, (conf.MaxMessages%100), last, "", "")
		if err != nil {
			fmt.Println("error reading extra messages: ", err)
			os.Exit(1)
		}
		messages = append(messages, tmp...)
	}

	End:
	if conf.Format == "csv" {
		exportAsCSV(messages, conf.Path)
	} else if conf.Format == "json" {
		exportAsJSON(messages, conf.Path)
	}
	os.Exit(0)
}

func exportAsJSON(messages []*discordgo.Message, path string) {
	jtxt, err := json.Marshal(messages)
	if err != nil {
		fmt.Println("error saving as JSON: ", err)
		os.Exit(1)
	}
	err = ioutil.WriteFile(path, jtxt, 0644)
	if err != nil {
		fmt.Println("error saving as JSON: ", err)
		os.Exit(1)
	}
}

func exportAsCSV(messages []*discordgo.Message, path string) {
	fmt.Println("CSV UNIMPLEMENTED")
	os.Exit(0)
	err := ioutil.WriteFile(path, []byte{}, 0644)
	if err != nil {
		fmt.Println("error saving as JSON: ", err)
		os.Exit(1)
	}
}